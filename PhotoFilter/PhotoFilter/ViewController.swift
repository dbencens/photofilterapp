

import UIKit
import AssetsLibrary
import Photos
import MobileCoreServices

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDelegateFlowLayout {
   let filter = CIFilter()
    
    @IBOutlet weak var photoImage1: UIImageView!
    
    let context = CIContext(options:nil)
    
    
    @IBAction func Applyfilter(sender: AnyObject) {
        
        
        let inputImage = CIImage(image: photoImage1.image)
        
        let randomColor = [kCIInputAngleKey: (Double(arc4random_uniform(314)) / 100)]
        
        let filteredImage = inputImage.imageByApplyingFilter("CIHueAdjust", withInputParameters: randomColor)
        let renderedImage = context.createCGImage(filteredImage, fromRect: filteredImage.extent())
        photoImage1.image = UIImage(CGImage: renderedImage)
    }
    
    @IBAction func Camera(sender: AnyObject) {
        
        if(UIImagePickerController.isSourceTypeAvailable(.Camera)){
            //loadcamerainterface
            var picker : UIImagePickerController = UIImagePickerController()
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.delegate = self
            picker.allowsEditing = false
            self.presentViewController(picker, animated: true, completion: nil)
            
        }else{
            
            
            //no camera avaliable
            var message = UIAlertController(title: "Error", message: "No camera avaliable", preferredStyle: .Alert)
            message.addAction(UIAlertAction(title: "Okay", style: .Default, handler: {(alertAction)in
                message.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(message, animated: true, completion: nil)
        }
    }
   
    @IBAction func Save(sender: AnyObject) {
        var error: NSError?
        if((error) != nil){
            
            
            //Will not work in simulator as it uses software rendering
            let imageSaved = filter.outputImage
            let contextS = CIContext(options:[kCIContextUseSoftwareRenderer: true])
            let CGimg = contextS.createCGImage(imageSaved, fromRect: imageSaved.extent())
            let library = ALAssetsLibrary()
            library.writeImageToSavedPhotosAlbum(CGimg, metadata: imageSaved.properties(), completionBlock: nil)
            
            
        }else{
        
    var alert1 = UIAlertController(title: "Error", message: "Can not save while in simulation", preferredStyle: .Alert)
        alert1.addAction(UIAlertAction(title: "Okay", style: .Default, handler: {(alertAction)in alert1.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert1, animated: true, completion: nil)
        }
    
    }
    
    
    
    @IBAction func DFilter(sender: AnyObject) {
        let inputImage = CIImage(image: photoImage1.image)
        let filter = CIFilter(name: "CIGaussianBlur")
            filter.setValue(inputImage, forKey: kCIInputImageKey)
               let create = context.createCGImage(filter.outputImage, fromRect: filter.outputImage.extent())
               let newD = UIImage(CGImage: create)
               self.photoImage1.image = newD
    }
    
    @IBAction func BLFilter(sender: AnyObject) {
        
        let inputImage = CIImage(image: photoImage1.image)
        let filter = CIFilter(name: "CIPhotoEffectTransfer")
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        let create = context.createCGImage(filter.outputImage, fromRect: filter.outputImage.extent())
        let newBL = UIImage(CGImage: create)
        self.photoImage1.image = newBL
    }
    
    @IBAction func AddImage(sender: AnyObject) {
        
     
        
        var addImage : UIImagePickerController = UIImagePickerController()
        addImage.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        addImage.delegate = self
        addImage.allowsEditing = false
        self.presentViewController(addImage, animated: true, completion: nil)
        
    }
    
   
    
    
    @IBAction func BlueEffect(sender: AnyObject) {
        
        let inputImage = CIImage(image: photoImage1.image)
        let filter = CIFilter(name: "CISepiaTone")
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        filter.setValue(1.0, forKey: kCIInputIntensityKey)
       let create = context.createCGImage(filter.outputImage, fromRect: filter.outputImage.extent())
        let newImage = UIImage(CGImage: create)
        self.photoImage1.image = newImage
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary!) {
    self.dismissViewControllerAnimated(true, completion: nil)
        let selectedImage = info[UIImagePickerControllerOriginalImage] as UIImage
       
        photoImage1.image = selectedImage
        
        
    }
    
    }

